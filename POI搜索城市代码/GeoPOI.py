#获取高德POI 2020/11/16 关键字搜索
#可以生产带坐标的.xlsx和在mysql中生成一个spatial_poi的数据库
#需要安装pymysql openpyxl
#安装mysql、安装excel
import json
import requests
import time
import pymysql
import openpyxl
class GaoDePOI(object):
    def __init__(self,api_key,keywords,types,city,citylimit,offset,pageCount,extensions):
        self.api_key=api_key
        self.keywords=keywords
        self.types=types
        self.city=city
        self.citylimit=citylimit
        self.offset=offset
        self.pageCount=pageCount
        self.extensions=extensions
    def urls(self):
        urls=[]
        for page in range(1,self.pageCount+1):
            url='https://restapi.amap.com/v3/place/text?key=' + self.api_key +\
                '&keywords='+self.keywords+'&types'+self.types+'&city='+str(self.city)+\
                '&citylimit='+self.citylimit+'&offset='+str(self.offset)+\
                '&page='+str(page)+'&output=json&key='+'&extensions='+self.extensions
            urls.append(url)
        return urls
    def WriteLocal(self,urls):
        connect = pymysql.connect(host='localhost', port=3306, user='mysql账号', passwd='mysql密码', db='spatial_poi')
        cursor = connect.cursor()
        cursor.execute("CREATE TABLE `plases`  (\
                              `ID` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,\
                              `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,\
                              Location GEOMETRY NOT NULL,\
                              `Address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,\
                              `Tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,\
                              `CityName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL\
                            ) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;")
        print("创建数据表成功！")
        excel=openpyxl.load_workbook("D://石家庄餐饮.xlsx")
        sheet=excel["石家庄市"]
        for url in urls:
            json_obj=requests.get(url).text
            data=json.loads(json_obj)
            if(data['status']=='1'):
                print(url+'\n'+'成功!')
            else:
                print('失败!')
            try:
                for item in data['pois']:
                    id = item['id']
                    exname = item['name']
                    if (str(item['name']).find("'") != -1):
                        name = str(item['name']).replace("'", "''")
                    else:
                        name = item['name']
                    Longitude, Latitude = item['location'].split(',')
                    Location = str(item['location']).replace(",", " ")
                    address = item['address']
                    if (len(item['address']) == 0):
                        address = ""
                    tag = item['tag']
                    if (len(tag) == 0):
                        tag = ""
                    cityname = item['cityname']
                    sheet.append([id, exname, Longitude, Latitude, address, tag, cityname])
                    sql = "INSERT INTO spatial_poi.plases(ID,Name,Location,Address,Tag,CityName) VALUES('{0}','{1}'," \
                          "st_geomfromtext('point({2})'),'{3}','{4}','{5}');".format(id, name, Location, address, tag, cityname)
                    cursor.execute(sql)
                    connect.commit()  # 提交创建内容
            except:
                print('错误！')
        cursor.close()
        connect.close()
        excel.save("D://石家庄餐饮.xlsx")
def CreateDataBase(createOrnot):
    if createOrnot:
        connect = pymysql.connect(host='localhost', port=3306,user='mysql账号', passwd='mysql密码', db='mysql')
        cursor = connect.cursor()
        cursor.execute("DROP DATABASE IF EXISTS spatial_poi")
        cursor.execute("CREATE DATABASE spatial_poi")
        print('创建数据库成功！')
        cursor.close()
        connect.close()
def CreateExcel(createOrnot,sheetName):
    if createOrnot:
        excel=openpyxl.Workbook()
        excel.create_sheet(sheetName)
        sheet=excel.active
        sheet=excel[sheetName]
        sheet.append(["ID","名称","经度","纬度","地址","特色标签","所在城市"])
        excel.save("D://石家庄餐饮.xlsx")
        print("创建Excel表成功！")
def main():
    #创建数据库
    CreateDataBase(True)
    #创建Excel
    CreateExcel(True,"石家庄市")
    gaode=GaoDePOI('Web应用申请的key', '美食', '餐饮服务', 130100, 'true', 20, 45, 'all')
    urls=gaode.urls()
    gaode.WriteLocal(urls)
if __name__ == '__main__':
    start=time.time()
    print("开始爬取")
    main()
    end=time.time()
    print("爬取结束******用时：{0}".format(end-start))